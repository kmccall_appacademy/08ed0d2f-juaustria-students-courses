class Student
  attr_reader :first_name, :last_name, :courses
  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    raise 'Error: course conflict' if @courses.any? do |enrolled| 
      enrolled.conflicts_with?(course)
    end
    @courses << course unless @courses.include?(course)
    course.students << self
  end

  def course_load
    output = Hash.new(0)
    @courses.each do |course|
      output[course.department] += course.credits
    end
    output
  end
end
