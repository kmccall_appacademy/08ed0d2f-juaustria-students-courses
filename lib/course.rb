# Made days and time_block arguments optional in order to pass all specs
class Course
  attr_reader :name, :department, :credits, :students, :days, :time_block
  def initialize(name, department, credits, *days_and_timeblock)
    @name = name
    @department = department
    @credits = credits
    @students = []
    @days = days_and_timeblock[0]
    @time_block = days_and_timeblock[1]
  end

  def add_student(student)
    student.enroll(self)
  end

  def conflicts_with?(course)
    same_days = course.days.select { |day| @days.include?(day) }
    @time_block == course.time_block && !same_days.empty?
  end
end
